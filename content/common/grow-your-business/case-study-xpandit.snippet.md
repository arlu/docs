<h2 class="title">Xpand IT</h2>

The company’s main objective for the first year was to get a big number of installations, product feedback, and customer reviews to refine and improve the product while gaining visibility on the Marketplace. “Once you reach a certain number of installations and you have a good average score in reviews, it starts growing by itself. You don’t actually need a big sales team,” says Pedro Gonçalves, co-founder and CTO.

<a href="/showcase/xpandit/">Learn more<i class="fa fa-arrow-right" aria-hidden="true"></i></a>