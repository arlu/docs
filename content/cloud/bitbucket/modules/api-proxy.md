---
title: "API proxy"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: modules 
date: "2018-06-12"
---
# API proxy

Used to create  a layer between your API and Bitbucket to provide the heavy lifting for authentication, parameter substitution, [object hydration](/cloud/bitbucket/proxy-object-hydration), and other services. You can read about how Bitbucket's API proxy works and how to use it in the [Proxy module guide](/cloud/bitbucket/proxy-module).

## Sample descriptor JSON

You create the proxy connection when you map a URL pattern to a specific destination URL in your app descriptor. The simplest form of this is:

``` json
"modules": {
    "proxy": {
        "/proxy-example/{repository}": {
            "destination": "/proxy-example?name={repository.owner.display_name}", // required
            "methods": { ... }, // optional
            "conditions": { ... }, // optional
            "scopes": { ... } // optional
        }
    }
}
```

In this example, the context parameter `{repository}` needs to be captured using the UUID. However, for objects that belong to another object -- like how a repository belongs to a user or team -- **the parent object needs to be resolved in order to resolve a child object**. Read more about this in the subsection of the article about Object hydration,  [Capturing the parent object of the object you're capturing](/cloud/bitbucket/proxy-object-hydration/#capture-parent-objects).

## Path definitions <a name="definitions"></a>

The path definition will be the URL that's transformed by Bitbucket and is served to clients.

### Properties <a name="properties"></a>

<table>
    <thead>
  <tr>
    <th colspan="3"></th>
  </tr>
  </thead>
  <tbody>
      <!-- DESTINATION -->
      <tr style="border-collapse: collapse; border: none;">
        <td colspan="3"><h3><code>destination</code></h3></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
          <td style="column-width: 2px"> </td>
          <td valign="top"><b>Type</b></td>
          <td>string</td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td></td>
        <td><b>Required</b></td>
        <td><span class="aui-lozenge aui-lozenge-error">Yes</span></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td></td>
        <td valign="top"><b>Description</b></td>
        <td>The destination for a proxy path. This can be a fully qualified, absolute URL, or it can be just a path. In case of the latter, it is concatenated to the base URL. Destination URLs can contain Connect context parameters that will be substituted at runtime with request properties.</td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td colspan="3"><h4>PROPERTIES</h4></td>
      </tr>
      <!-- METHODS -->
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top" rowspan="3"><code>methods</code></td>
        <td valign="top"><b>Type</b></td>
        <td>string</td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top"><b>Required</b></td>
        <td valign="top"><span class="aui-lozenge">No</span></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top"><b>Description</b></td>
        <td colspan="2">The HTTP methods GET, PUT, POST and DELETE can be defined inside the <code>methods</code> property in order to explicitly define which methods are can be forwarded. Within this method definition, <code>destination</code>, <code>scopes</code> or <code>conditions</code> can be defined as properties to override the properties at the top level for the path definition.</td>
      </tr>
      <!-- SCOPES -->    
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top" rowspan="3"><code>scopes</code></td>
        <td valign="top"><b>Type</b></td>
        <td>[<a href="/cloud/bitbucket/bitbucket-cloud-rest-api-scopes">Scopes</a>]</td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top"><b>Required</b></td>
        <td><span class="aui-lozenge">No</span></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top"><b>Description</b></td>
        <td>The scopes property can be used to define the scopes a request needs in order to be able to access the resource. For more details refer to the list of <a href="/cloud/bitbucket/bitbucket-cloud-rest-api-scopes">Rest API Scopes.</a></td>
      </tr>
      <!-- CONDITIONS -->
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top" rowspan="3"><code>conditions</code></td>
        <td valign="top"><b>Required</b></td>
        <td><span class="aui-lozenge">No</span></td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
          <td valign="top"><b>Type</b></td>
          <td>[<a href="/cloud/bitbucket/modules/simple-condition/">Simple condition</a>, <a href="/cloud/bitbucket/modules/composition-condition/">Composition condition</a>]</td>
      </tr>
      <tr style="border-collapse: collapse; border: none;">
        <td valign="top"><b>Description</b></td>
        <td>An array of <a href="/cloud/bitbucket/conditions/">conditions</a> used to restrict access to the resource.</td>
      </tr>
      <tr style="border-collapse: collapse">
        <td colspan="3"> </td>
      </tr>
  </tbody>
</table>

</br>

{{% warning title="Declaring conflicting conditions or scopes" %}}

If nested scopes or conditions conflict, values declared by a child element will override values declared by parent elements. See the [Restrict HTTP methods example](/cloud/bitbucket/proxy-module/#proxy-example-restricthtttpmethods) in the API proxy module guide for an example of conflicting nested conditions.

{{% /warning %}}