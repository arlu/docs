# User Privacy App Developer Guide

<div class="aui-message note">
    <div class="icon"></div>
    <p>
        <strong>Early Access</strong>
    </p>
    </br>
    <p>
      Please note that this guide has been released for early access. 
      The APIs referenced are still under development and are subject to change.
    </p>
    <br/>
</div>

## Introduction

All App developers are responsible for ensuring their apps comply with 
[GDPR requirements](https://ec.europa.eu/commission/priorities/justice-and-fundamental-rights/data-protection/2018-reform-eu-data-protection-rules_en). 
If an App stores user personal data in any form, it must remove that data 
if requested by the user 
([right to erasure](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/right-to-erasure/), 
also known as right to be forgotten). It must also either erase or update any 
personal data when changed by the user 
([right to rectification](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/right-to-rectification/)). 
For this reason Atlassian recommends that Apps do not store any user personal data 
and always retrieve current user data at the time of use using Atlassian APIs.

GDPR requirements also specify that users have a 
[right to be informed](https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/right-to-be-informed) 
about the collection and use of their personal data. To satisfy these requirements, 
Atlassian has created an API that must be used by apps which are storing personal data.

Based on apps using this API, users will be able to see which apps are storing their 
data by visiting their Atlassian Account profile.

## Definitions

The following definitions are used in this guide:

*   **accountId**: This is a short hand reference to Atlassian Account ID. These IDs 
are between 1-128 characters long, and contain alphanumeric characters as well as “-” 
and “:” characters.
    
*   **Cycle period**: The nominal duration of time between sending reports for a 
given accountId. This period implies the maximum staleness of the report data that 
is persisted within Atlassian.
    
*   **Polling iteration period**: This is the period over which an app will iterate 
over all the accountIds to send reports for personal data usage.
    

## Personal Data Reporting API

The API requires Apps to periodically report which users they are storing personal 
data for and respond to directives to erase personal data when necessary. The API 
is RESTful and comprises multiple resources that are aligned with various existing 
Atlassian APIs and authentication schemes such as 
[JSON Web Tokens (JWT)](../authentication-for-apps/) 
for Jira and Confluence, and 
[Jira 3-Legged OAuth](../oauth-2-authorization-code-grants-3lo-for-apps/). 
This provides the simplest means for existing apps to be extended call the new polling resources.

For flexibility and efficiency, the API allows multiple accounts to be reported on 
in a single request.

### Cycle Period

The nominal cycle period is 15 days, however, responses may be returned with 
a **Cycle-Period** header to indicate the cycle period in days. Apps are expected 
to always read and use the directed cycle period.

### Rate Limiting

To limit the load on servers, apps should not send reports on accountIds more frequently 
than the cycle period. This means that apps should obey the following rate limit headers:

**Retry-After**: This header is provided for 429 status responses. It indicates the 
minimum number of seconds to delay before re-attempting the API call.

### Batch Sizes

The number of accounts in each request can be from 1 to 90 inclusive.

### App Implications

#### Personal Data Age

Apps must track the age of the personal data retrieved from Atlassian. This must be 
sent in reports so that Atlassian can determine if the personal data is stale. If an 
app stores multiple aspects of personal data for an account, the age must correspond 
to the oldest time that the personal data was retrieved at.

#### Interruptions

The longer the polling iteration period, the more likely it will be that the polling 
may be interrupted by events such as server restarts and app updates. This means 
apps need to persistently keep track of where they are within a poll period and have 
a means of resuming a poll cycle from this information. This extra complication may 
be a significant barrier for apps, in which case a shorter polling iteration period 
is the pragmatic solution.

#### Scheduling

Do not schedule polling requests to take place at a specific time of day such as midnight 
or at the top of each hour since this may also coincide with other apps with similar 
schedules.

#### Account Additions and Deletions

The iteration logic must handle account additions and deletions. This will necessitate 
adjustments to the polling rate and/or batch sizes.

#### Addressability and Encapsulation

It is imperative that apps are able to report personal data usage accurately and 
reliably. To ensure all personal data is erased when necessary, it is recommended 
that the app only stores a single copy of the personal data within this address 
space and retrieves it when necessary. The obvious choice for an address space is 
a persistent store such as a database table that supports efficient querying by 
accountId.

#### Handling Uninstall Events

When an app is uninstalled, or consent is revoked in the case of 3LO, it should 
erase personal data that will no longer be needed.

### Jira and Confluence JWT Polling API

#### Description

This API is intended for Jira and Confluence Connect apps. 
The API allows apps to report for which accounts they are storing personal data.
The app indicates the date at which the personal data for each account was captured. 
The response will indicate if data should be removed or refreshed.

#### Method and URL

POST /rest/atlassian-connect/latest/report-accounts

#### Authentication

Standard Jira and Confluence JWT authentication. See the authentication section of the 
[REST API](../rest/).

#### Request

Each request allows between 1 and 90 accounts to be reported on. For each account, the 
accountId and time that the personal data was retrieved must be provided. The time is 
formatted as an 
[ISO8601 date](https://www.iso.org/iso-8601-date-and-time-format.html).

Content type: application/json

Example request:

``` json
{
"accounts": [{
    "accountId": "account-id-a",
    "updatedAt": "2017-05-27T16:22:09.000Z"
  }, {
    "accountId": "account-id-b",
    "updatedAt": "2017-04-27T16:23:32.000Z"
  }, {
    "accountId": "account-id-c",
    "updatedAt": "2017-02-27T16:22:11.000Z"
  }]
}
```

#### Responses

##### 200 Response

A 200 response indicates the request is successful and one or more personal data erasure 
actions are required. The information is contained in an `accounts` array where each object 
identifies the accountId and whether the reason for the erasure is due to the closure of 
the account or invalidation of the app’s copy of personal data due to the some update. In 
the case of the latter, the app is permitted to re-request personal data.

Content type: application/json

Example response:

``` json
{
  "accounts": [{
    "accountId": "account-id-a",
    "status": "closed"
  }, {
    "accountId": "account-id-c",
    "status": "updated"
  }]
}
```

##### 204 Response

A 204 response indicates the request is successful and no action by the app is required 
with respect to the accounts sent in the request.

##### 400 Response

A 400 response indicates the request was malformed in some way. The response body will 
contain an error message.

Example response:

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```

###### 403 Response

A 403 response indicates the request is forbidden.

##### 429 Response

A 429 response is used for rate limiting.
The app must follow the rate limiting directives provided in response headers.

##### 500 Response

A 500 response indicates an internal server error occurred. 
The response body will contain an error message.

Example response:

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```

##### 503 Response

A 503 response indicates that the service is currently unavailable.
The service may be unavailable during initial development or due to an outage of an upstream dependency.

### 3LO Polling API

#### Description

This API is intended for Jira and Confluence 3LO apps.
The API allows apps to report for which accounts they are storing personal data.
The app indicates the date at which the personal data for each account was captured. 
The response will indicate if data should be removed or refreshed.

#### Method and URL

POST https://api.atlassian.com/app/report-accounts/

#### Authentication

OAuth 2 3LO.

#### Request

The request has no parameters.

Example request:

``` json
{
  "accounts": [
    {
      "accountId": "string",
      "updatedAt": "2018-10-25T23:08:51.382Z"
    }
  ]
}
```

#### Responses

##### 200 Response

A 200 response indicates the request is successful and one or more personal data erasure 
actions are required. The information is contained in an `accounts` array where each object 
identifies the accountId and whether the reason for the erasure is due to the closure of 
the account or invalidation of the app’s copy of personal data due to the some update. In 
the case of the latter, the app is permitted to re-request personal data.

Content type: application/json

Example response:

``` json
{
  "accounts": [{
    "accountId": "account-id-a",
    "status": "closed"
  }, {
    "accountId": "account-id-c",
    "status": "updated"
  }]
}
```

##### 204 Response

A 204 response indicates the request is successful and no action by the app is required 
with respect to the accounts sent in the request.

##### 400 Response

A 400 response indicates the request was malformed in some way. The response body will 
contain an error message.

Example response:

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```

##### 429 Response

A 429 response is used for rate limiting. 
The app must follow the rate limiting directives provided in response headers.

##### 500 Response

A 500 response indicates an internal server error occurred. 
The response body will contain an error message.

Example response:

``` json
{
  "errorType": "string",
  "errorMessage": "string"
}
```



###### 503 Response

A 503 response indicates the service is unavailable.

## Q & A

1.  When is an app obliged to use the personal data reporting API?  
    All apps storing personal data must use the personal data reporting API.
    
2.  In order to prevent stale data in Atlassian, does my app need to report when 
it pro-actively erases personal data for a user?  
    No. Atlassian will apply a time to live (TTL) on the data reported by apps. This 
    means an app may be listed in a user's profile for some period after the app has 
    ceased storing personal data for that user.
    