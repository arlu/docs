---
title: "Security overview"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: security
date: "2016-07-22"
---

# Security overview

Atlassian Connect security enables us to protect customer data from unauthorized access and from malicious or accidental changes,
thus allowing administrators to install and evaluate apps, and users to leverage installed apps in a secure manner.

Atlassian Connect's security goals are to:

* identify apps,
* work out what each app can do,
* accept allowed actions with minimal friction,
* clearly reject disallowed actions,
* help administrators installing apps to understand what the apps can do, and
* allow administrators to arbitrarily limit the actions that each app can take.

There are two main branches of security, [authentication](/cloud/bitbucket/authentication-for-apps) and authorization, with authorization
further divided into [static](/cloud/bitbucket/bitbucket-cloud-rest-api-scopes) and run-time authorization.

## Authentication

Authentication tells the product the identity of the app and is the basis of all other security. On each incoming API
request the identity of the app is established, with requests that do not contain authentication information being
treated as anonymous.

Read more details in our [authentication documentation](/cloud/bitbucket/authentication-for-apps).

## Authorization

Now that the app sending each incoming request has been identified we need to work out if the app is allowed to
make this request, allowing authorized actions and rejecting unauthorized actions. We want to help
administrators installing apps to understand what each app can do and also allow them to arbitrarily limit the
potential actions of individual apps.

### Static authorization: Scopes

An app must declare the maximum set of actions that it may perform: read, write, etc. This security level is enforced
by Connect and cannot be bypassed by app implementations. Therefore administrators can be assured that the apps with
"read" permission can only read data and not modify it, and that apps with "write" permission cannot delete data, etc.

App vendors cannot possibly know about the myriad product instances into which the app may be installed, so they
need a way of statically specifying the maximum set of actions that their app may perform. This is expressed in the
scopes in the app descriptor and is presented to the administrator during installation.

Read more details in our [scopes documentation](/cloud/bitbucket/bitbucket-cloud-rest-api-scopes).

### Run-time authorization: App Users

Some administrators have some content that they wish to protect from apps. For example, some companies store sensitive
details such as payroll and future business plans in product instances, and wish to be assured that it is accessible to
few apps or no apps at all. Without this assurance, such a company would likely install no apps.

This protected data varies across instances, as does its protections. Atlassian and app vendors have no way of knowing
in advance what individual administrators will decide must be protected from apps nor what the protections should be
(for example, perhaps one app is allowed read access to one particular super-secret-project while another is allowed
write access).

We enable administrators to arbitrarily limit app access to user data at run-time by assigning every app its own
user. Administrators can then permission these app users in very similar ways to how they permission normal users.
Every incoming server-to-server request from a Connect app is assigned the app's
user and authorization proceeds as normal from that point onwards, with the app user's permissions limiting what API
features the incoming requests may target. Apps are not told which user they are assigned and do not need to specify
it: it is automatically mapped from the identity of the app itself.

Currently, in-browser requests are assigned the user at the browser.

### Combining Static and Run-time Authorization

The set of actions that an app is capable of performing is the intersection of the static scopes and the permissions
of the user assigned to the request. It is entirely possible that any request may be rejected because the assigned user
lacks the necessary permission, so the app should always defensively detect
[HTTP 403 forbidden](http://en.wikipedia.org/wiki/HTTP_403) responses from the product and, if possible, display an
appropriate message to the user.


### Road Map

Currently, the set of actions that an app is capable of performing is limited to the intersection of its scopes and the
app user's permissions (for server-to-server) or to the intersection of its scopes and the in-browser user's
permissions (for in-browser requests).

We will ultimately move to a model that additionally respects the app user's permissions on in-browser requests and
helps apps to be good citizens in server-to-server requests by allowing apps to specify the user on whose behalf
they are acting (if acting on behalf of a specific user, which will not always be the case in server-to-server requests).

The set of allowed actions will then be limited to the intersection of the app's scopes, the app user's permissions
and (if there is a context user) the context user's permissions.
