---
title: "Showcase Adaptavist"
logo_url: /img/logo-adaptavist-white.png
heading1: "Adaptavist bets on cloud"
byline: "ScriptRunner for Jira builds on its success with Atlassian's cloud products"
lede: "Established in 2005, Adaptavist helps the world’s most complex enterprises optimize their application lifecycle using Atlassian software. With clients in over 60 countries, Adaptavist is a leading provider of Atlassian-focused professional services, managed services, training, and apps to more than half of the Fortune 500. Adaptavist is an Atlassian Platinum Solution Partner for the Enterprise and an authorized Atlassian Training Partner."
---

Jamie Echlin didn’t build ScriptRunner for Jira to build a successful Marketplace business; he built it to solve a problem. That problem was that he was required to use a different Java app every time he wanted to create an automation in Jira or integrate with another application. Frustrated with the inability to scale, he created ScriptRunner for Jira, a collection of powerful yet easy-to-use workflow functions, JQL functions, listeners, and services using Groovy.

Seven years later, Jamie teamed up with Atlassian Platinum Solution Partner&mdash;and long-time fans of ScriptRunner&mdash;Adaptavist. Since 2014, Jamie and his team at Adaptavist have transformed ScriptRunner for Jira into a multi-million dollar business, with more than 17,000 installs around the globe and customers which include some of the world’s leading enterprise brands.

“If you’ve written something useful to you, which is how most apps start, it’s going to be useful for other people. You’re certainly not the only person with the problem that it’s solving,” says Jamie.

## Extending the power of automation to the cloud
  
The introduction of cloud products significantly grew and broadened Atlassian’s user base. The ScriptRunner product team began investigating what level of effort it would take to offer automation and integration capabilities for Jira Cloud to reach potential new customers.

While Atlassian cloud products may share the same outward functionality and use cases, the underlying technology is very different for cloud. In short, because cloud does not enable synchronous operations, it was evident that developing for it would require ScriptRunner for Jira  to be comprehensively re-engineered.

That question of feasibility was finally settled through an Adaptavist ShipIt Day competition. The ScriptRunner team succeeded in creating a proof of concept application. Having demonstrated its viability, a dedicated team was assembled to port ScriptRunner for Jira to the cloud, rather than attempt to fork the development of the existing server product. They opted to use the non-blocking Ratpack JVM framework, backed by Java and Netty, to build a low latency service that can scale to millions of script executions a day.

## Challenges and benefits in developing for the cloud

Visibility of information across instances and the immediacy of feedback in cloud brings many benefits, like being able to quickly find and fix problems, make product adjustments, or prioritize feature releases to reflect actual usage and customer demand.

Jon Mort leads the ScriptRunner for Jira Cloud team at Adaptavist. “Operating as a SaaS has its demands. Cloud increases your exposure to external events. However, the ability to monitor important metrics across all instances ensures increased tolerance to failure and allows us to better mitigate usage spikes or outages with minimal service degradation.”

“Undoubtedly hurdles will arise when you’re taking a successful, feature-rich product like ScriptRunner from server to cloud. One of these is meeting customer expectations when some functionality simply cannot be replicated.” Despite this, Jon believes “the centralized and harmonized nature of the cloud offers such a wealth of opportunities that it more than compensates for any development challenges.” 

## Beating expectations

Since ScriptRunner for Jira Cloud launched in September 2016, adoption of the product has beaten expectations. With 58% customer growth and 105% revenue growth in under one year, Adaptavist has found the cloud business to be a profitable venture in a short amount of time.

As the Connect framework matures and more innovation allows for more feature parity, the roadmap for Adaptavist cloud apps is set to bring even more powerful automation to the cloud. Expect cloud versions of apps for the other Atlassian products from Adaptavist soon.

{{% linkblock href="../xpandit/" img="/img/chat-bubble.png" text="Xpand IT" section="server" align="right" %}}
{{% linkblock href="../wittified/" img="/img/clouds.png" text="Wittified" section="cloud" align="left" %}}
