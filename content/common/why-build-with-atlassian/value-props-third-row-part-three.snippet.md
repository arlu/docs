<h4 class="center">Now you're part of the community</h4>

Need help building with us? We have a large developer community that is always there for you. Get help online through our <a href="https://community.developer.atlassian.com/t/welcome-to-the-community/84" target="_blank">community forum</a> , or get help in person by attending <a href="https://www.atlassian.com/company/events/summit-europe/programs" target="_blank">AtlasCamp.</a>
