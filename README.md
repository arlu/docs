# Atlassian developer documentation

This repository contains content for developer.atlassian.com (DAC). Content is written in Markdown 
and using the [OpenAPI Specification](https://swagger.io/docs/specification/about/) (formerly known 
as Swagger).

## Contributors

Pull requests, issues, and comments welcome. For pull requests:

- Follow the existing style. See [Voice and tone](https://atlassian.design/guidelines/voiceAndTone/language-grammar) for guidance.
- Separate unrelated changes into multiple pull requests.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves 
as a record stating that the contributor is entitled to contribute the code, documentation, or 
translation to the project and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

You will be asked to sign the CLA during the process of creating your pull request. 

## Atlassian contributions 

In addition to the guidance above, follow all guidelines in the 
[Writing toolkit](https://developer.atlassian.com/platform/writing-toolkit/).

You can preview your changes as you make them by following
[this guide](https://developer.atlassian.com/dac/viewing-docs-locally/).

Note that the *Writing toolkit* is restricted to Atlassian employees, and you will need to log in 
with your Atlassian email address to access it. 

## License

Copyright (c) 2016-2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.