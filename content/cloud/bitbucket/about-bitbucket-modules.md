---
title: "About Bitbucket modules"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: modules
date: "2016-04-28"
---

# About Bitbucket modules

Modules are the specific integration points implemented by your app. These include UI elements like pages, web panels, and web items. Other types of modules do not modify the Bitbucket UI, but describe how your app interacts programmatically with Bitbucket, such as webhooks and oauth consumers.