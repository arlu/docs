---
title: "Dialog"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: javascript 
date: "2017-05-18"
---

# Dialog

The Dialog module provides a mechanism for launching an app's modules as modal dialogs from within an app's iframe.

A modal dialog displays information without requiring the user to leave the current page. The dialog is opened over the entire window, rather than within the iframe itself.

## Styling your dialog to look like a standard Atlassian dialog

By default the dialog iframe is undecorated. It's up to the developer to style the dialog.

![styled dialog](/cloud/bitbucket/images/connectdialogchromelessexample.jpeg)

In order to maintain a consistent look and feel between the host application and the app,
we encourage app developers to style their dialogs to match Atlassian's Design Guidelines for modal dialogs.

To do that, you'll need to add the AUI styles to your dialog. For more information, read about the Atlassian User Interface [dialog component](https://docs.atlassian.com/aui/latest/docs/dialog2.html).
    
## Classes

### DialogButton
    
A dialog button that can be controlled with JavaScript</p>

### DialogOptions

#### Properties

<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    </thead>

    <tbody>
        <tr>
                <td><code>key</code></td>

            <td>String</td>

            <td>The module key of the page you want to open as a dialog</td>
        </tr>
        <tr>
                <td><code>size</code></td>
            <td>String</td>

            <td>Opens the dialog at a preset size: small, medium, large, x-large, or maximum (full screen)</td>
        </tr>
        <tr>
                <td><code>width</code></td>
            <td>Number | String</td>

            <td>overrides size, defines the width as a percentage (append a % to the number) or pixels</td>
        </tr>
        <tr>
                <td><code>height</code></td>
            <td>Number | String</td>
            <td>overrides size, defines the height as a percentage (append a % to the number) or pixels</td>
        </tr>
        <tr>
                <td><code>chrome</code></td>
            <td>Boolean</td>

            <td>(optional) opens the dialog with heading and buttons</td>
        </tr>
        <tr>
                <td><code>header</code></td>
            <td>String</td>
            <td>(optional) text to display in the header if opening a dialog with Chrome</td>
        </tr>
        <tr>
                <td><code>submitText</code></td>

            <td>String</td>
            <td>(optional) text for the submit button if opening a dialog with Chrome</td>
        </tr>
        <tr>
                <td><code>cancelText</code></td>
            <td>String</td>
            <td>(optional) text for the cancel button if opening a dialog with Chrome</td>
        </tr>
    </tbody>
</table>


## Methods

### close (data)
    
Closes the currently open dialog. Optionally pass data to listeners of the `dialog.close`. This will only close a dialog that has been opened by your app. You can register for close events using the `dialog.close` event and the [events module](/cloud/bitbucket/jsapi/events/).

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>

        <tr>
                <td><code>data</code></td>
            <td>Object</td>
    <td>An object to be emitted on dialog close</td>
        </tr>
	
	</tbody>
</table>


#### Example
          
``` javascript
AP.require('dialog', function(dialog){
  dialog.close({foo: 'bar'});
});
```

### create options

Creates a dialog for a web-item or page module key

#### Parameters   

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th >Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>options</code></td>
            <td>DialogOptions</td>
            <td>configuration object of dialog options</td>
        </tr>
	</tbody>
</table>
 
#### Example
        
``` javascript
AP.require('dialog', function(dialog){
  dialog.create({
    key: 'my-module-key',
    width: '500px',
    height: '200px'
  });
});
```

### getButton() &rarr; {DialogButton}
    
Returns the button that was requested (either `submit` or `cancel`).

**Returns**: [DialogButton](#dialogbutton)
    
#### Example
        
``` javascript
AP.require('dialog', function(dialog){
  dialog.getButton('submit');
});
```

### onDialogMessage (String, Function)

Register callbacks responding to messages from the host dialog, such as `submit` or `cancel`.

#### Parameters

<table>
    <thead>
	<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Description</th>
	</tr>
	</thead>
	<tbody>
        <tr>
                <td><code>String</code></td>
            <td>
            </td>

            <td>button either <code>cancel</code> or <code>submit</code></td>
        </tr>
        <tr>
                <td><code>Function</code></td>
            <td>
            </td>

            <td>callback function</td>
        </tr>
	</tbody>
</table>

{{% note %}}This functionality is deprecated and may be removed in the future.{{% /note %}}
 