---
title: Project sidebar
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
date: "2017-09-29"
---

{{< reuse-page path="docs/content/cloud/jira/platform/jira-project-sidebar.md">}}