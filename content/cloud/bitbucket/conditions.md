---
title: "Conditions"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: blocks 
date: "2015-12-01"
---

# Conditions

A condition specifies requirements that must be met for a user to access the features or UI exposed by a module. For
instance, the condition can require a user to be an administrator, have edit permissions, and apply other requirements
for access. If the condition is not met, the panel, page, or other UI element exposed by the app does not appear on
the page.

Various types of modules accept conditions, including `generalPages`, `adminPages`, and `webItems`. To see whether a certain
module accepts conditions, see their specific module documentation page.

The following conditions are currently available: 

* [has_account_permission](#has-account-permission): determines the user's account access level.  
* [has_permission](#has-permission): determines if a user has permission to access the module. 
* [scm](#scm): determines the SCM system (Git or Mercurial) for a module. 


### has_account_permission 

This condition restricts access to the modules based upon user permission settings for the account and can have the following parameters: 

* `collaborator`: is a member of the team but not an administrator. 
* `admin`: is an administrator for the team. 

``` javascript
{
    "name": "My App",
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "has_account_permission",
                        "invert": true,
                        "params": {
                            "permission": "collaborator"
                        }
                    }
                ]
            }
        ]
    }
}
```

### has_permission 

This condition restricts access to the modules based upon user permission settings for the repository and has the following parameters: 

* `read`: has read access to the repository. 
* `write`: has write access to the repository. 
* `admin`: has administrator access to the repository. 


``` javascript
{
    "name": "My App",
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "has_permission",
                        "invert": false,
                        "params": {
                            "permission": "admin"
                        }
                    }
                ]
            }
        ]
    }
}
```

### scm


This condition checks to see if the repository in question is Git or Hg and has the following parameters: 

* `git`: To verify the repository is a Git repository. 
* `hg`: To verify the repository is Mercurial (Hg).


``` javascript
{
    "name": "My App",
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "scm",
                        "params": {
                            "type": "git"
                        }
                    }
                ]
            }
        ]
    }
}
```

