---
title: "Using the project page module"
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
guides: guides
date: "2017-11-29"
---
# Using the project page module

The project page module allows apps to provide functionality in the context of Jira projects. These represent pages accessible from the Jira project sidebar.

This guide will show you how to use the Jira [project page module]. We'll take you on a guided tour of an example app, `jira-project-page-module-demo`, that uses the project page module and explain the key concepts.  You won't be building an app yourself in this guide, but you can browse, download, and even run the source code for the example app.

{{% note %}} The code for the example app described in this guide is here: [https://bitbucket.org/atlassianlabs/jira-project-page-module-example](https://bitbucket.org/atlassianlabs/jira-project-page-module-example).{{% /note %}}

The `jira-project-page-module-demo` app that we will be looking at, uses the project page module to configure contacts of a customer representative associated with each project in an external organisation.

## Before you begin

This guide is aimed at developers who are experienced with Jira Cloud development and the Connect framework. If you are new to either of these, we recommend that you read our [Getting started guide] first.

## Introducing customer contacts for projects

In our example we are going to edit the information of a customer representative, who is our contact person for some project. We would like to use different fields for customers in Software project and Business projects, so we will need a way of determining the project type when our page loads.

## "Customer contacts" pages

Let's assume that our Jira instance is used to manage project delivery for some external customers, and for each project we're in touch with selected customer representative. In order to have easy access to contact information of the customer representative, we will build an app that stores contact details in the context of each project. For Business projects we are going to store 3 fields: contact name, email and organisation name. For Software projects we will need one more field: person role. In order to separate the two types of customer contacts we are introducing two different page modules - one for Business and one for Software projects.

![Customer contacts page for Business project](../images/jiradev-projectpage-contactsbusiness.png)

*Customer contacts page for Business project*

![Customer contacts page for Software project](../images/jiradev-projectpage-contactssoftware.png)

*Customer contacts page for Software project*

The contact details are going to be loaded from app server application before displaying the page, and stored when pressing "Store contact" button. 

You can see the page templates in [contacts.hbs] and [contacts-software.hbs] files. The page form, which handles the logic, is in [index.js] and [contacts.js] files.

## Defining project page module

A project page module is declared in the app descriptor. In our example we use the following declaration:

``` json
"jiraProjectPages": [
    {
        "key": "edit-project-customer-contacts",
        "name": {
            "value": "Contacts"
        },
        "url": "/contacts?projectKey=${project.key}",
        "iconUrl": "/images/link.svg",
        "weight": 1,
        "conditions": [
            {
                "condition": "project_type",
                "invert": true,
                "params": {
                    "projectTypeKey": "software"
                }
            }
        ]
    },
    {
        "key": "edit-project-customer-contacts-software",
        "name": {
            "value": "Contacts"
        },
        "url": "/contacts-software?projectKey=${project.key}",
        "iconUrl": "/images/link.svg",
        "weight": 2,
        "conditions": [
            {
                "condition": "can_use_application",
                "params": {
                    "applicationKey": "jira-software"
                }
            },
            {
                "condition": "project_type",
                "params": {
                    "projectTypeKey": "software"
                }
            }
        ]
    }
]
``` 

Let's have a closer look at the properties of this module:

-   The `key` is the unique identifier of the module. This is an arbitrary value, that can contain alphanumeric characters as well as dashes, but cannot exceed 32 characters. Note, that this key will be used when accessing the field via the REST API.
-   The `name` is the text that is going to be used for the link in the project navigation sidebar. It can also be set to any value that you want, but note that this is what the app users will see in the project sidebar. 
-   The `url` references the page on app server, which is going to be displayed as the content of the project page. Here we pass `project.key` so that app server could associate a request with a proper project.
-   The `iconUrl` is a URL address of the image, which will be used as an icon for the app in the project navigation sidebar.
-   The `weight` defines an order of project page links provided by apps.
-   The `conditions` field can be used to restrict page link for certain scenarios according to [Conditions] page.

Here we described two project page modules: `edit-project-customer-contacts` for editing customer information in Business projects, mapped to app server's `/contacts` page, and `edit-project-customer-contacts-software` for editing customer information in Software projects, mapped to `contacts-software`. Each module uses conditions to differentiate between the two cases: either project should not be of Software type, or project should be of Software type and the user should have access to Software projects.

You can see this module definition in the [atlassian-connect.json] file of the `jira-project-page-module-demo` app.

## Testing the pages

To test our project pages you would need two projects - a Business one and a Software one.

Create Business project, navigate to it's view page and select the link `Contacts` from the project navigation sidebar.  Now create a Software project, and navigate to the same link as before. You would see that now the form contains additional `Role in process` field. The two pages actually represent two different pages on the app server. You can edit information for both pages to confirm that they are stored separately for both projects. 

{{% tip title="Congratulations!"%}}You now know how to implement a project page in a Jira Cloud app.{{% /tip %}} 

## Next steps

If you've finished this tutorial, check out other [Jira modules].

  [atlassian-connect.json]: https://bitbucket.org/atlassianlabs/jira-project-page-module-example/src/f20d1010f4917fd1a78e0e4baa2f7e44e5d430be/atlassian-connect.json?at=master
  [contacts.hbs]: https://bitbucket.org/atlassianlabs/jira-project-page-module-example/src/f20d1010f4917fd1a78e0e4baa2f7e44e5d430be/views/contacts.hbs?at=master
  [contacts-software.hbs]: https://bitbucket.org/atlassianlabs/jira-project-page-module-example/src/f20d1010f4917fd1a78e0e4baa2f7e44e5d430be/views/contacts-software.hbs?at=master
  [index.js]: https://bitbucket.org/atlassianlabs/jira-project-page-module-example/src/f20d1010f4917fd1a78e0e4baa2f7e44e5d430be/routes/index.js?at=master
  [contacts.js]: https://bitbucket.org/atlassianlabs/jira-project-page-module-example/src/f20d1010f4917fd1a78e0e4baa2f7e44e5d430be/routes/contacts.js?at=master
  [Jira Cloud development]: https://developer.atlassian.com/display/jiracloud
  
  [project page module]: /cloud/jira/platform/modules/project-page/
  [Getting started guide]: /cloud/jira/platform/getting-started
  [jiraProjectAdminTabPanels]: /cloud/jira/platform/modules/project-admin-tab-panel/
  [Jira modules]: /cloud/jira/platform/about-jira-modules
  [Conditions]: /cloud/jira/platform/conditions
