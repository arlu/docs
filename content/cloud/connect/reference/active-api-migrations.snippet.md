

The following API Migrations are currently active (where active means their value will can affect API behavior):

|Migration Key|Purpose / Changed behavior|Allowed values|
|---|---|---|
| `gdpr` | Controls whether personal data is provided via the normal Connect APIs. |<ul><li>[omitted]: This means personal data will be included in certain APIs until the end of the deprecation period.</li><li>`true`: This means personal data will be excluded from certain APIs.</li> </ul>|
