---
title: Connect API migration
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
date: "2018-09-01"
aliases:
- /cloud/jira/platform/connect-active-api-migrations.html
- /cloud/jira/platform/connect-active-api-migrations.md
---
{{< include path="docs/content/cloud/connect/reference/active-api-migrations.snippet.md">}}
