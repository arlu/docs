# Release Notes

## <a name="1.2.42" href="#1.2.42">1.2.42</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.42")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.41" href="#1.2.41">1.2.41</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.41")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.38" href="#1.2.38">1.2.38</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.38", "Connect-1.2.38")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.37" href="#1.2.37">1.2.37</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.37")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.36" href="#1.2.36">1.2.36</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.36", "Connect-1.2.36")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.35" href="#1.2.35">1.2.35</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.35", "Connect-1.2.35")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.34" href="#1.2.34">1.2.34</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.34")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.32" href="#1.2.32">1.2.32</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.32", "Connect-1.2.32")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.31" href="#1.2.31">1.2.31</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("Connect-1.2.31")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.28" href="#1.2.28">1.2.28</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("Connect-1.2.28")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.26" href="#1.2.26">1.2.26</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.26")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.25" href="#1.2.25">1.2.25</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.25")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.23" href="#1.2.23">1.2.23</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.23")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.22" href="#1.2.22">1.2.22</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.22")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.19" href="#1.2.19">1.2.19</a>
<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.19", "Connect-1.2.19")) ORDER BY priority DESC, component ASC, key ASC'></div>


## <a name="1.2.15" href="#1.2.15">1.2.15</a>

## New Jira user role condition

In order to solve [AC-2067](https://ecosystem.atlassian.net/browse/AC-2067) we have added a new
`entity_property_contains_any_user_role` condition. This is very similar to the
`entity_property_contains_any_user_group` condition. It is aliased by the `addon_property_contains_any_user_role`
condition, for which the `entity` parameter is omitted.

This condition allows you to check whether or not the browser user has any role specified in that entity
property, which must be an array of strings. For example, you may have an issue entity property called `myConfig`
containing the following value:

    {
        "myListOfRoles": [ "developers", "support-staff" ]
    }

Then you could write the following condition to restrict the display of a module to a user with at least one of
those roles:

    {
        "condition": "entity_property_contains_any_user_role",
        "params": {
            "entity": "issue",
            "propertyKey": "myConfig",
            "objectName": "myListOfRoles"
        }
    }

## <a name="1.2.10" href="#1.2.10">1.2.10</a>

## New Jira & Confluence user group membership conditions

In order to solve [AC-804](https://ecosystem.atlassian.net/browse/AC-804) we have added a new
`entity_property_contains_any_user_group` condition. This condition has the following aliases:

 - `addon_property_contains_any_user_group`
 - `content_property_contains_any_user_group`
 - `space_property_contains_any_user_group`

This condition allows you to check whether or not the browser user is a member of any group specified in that entity
property, which must be an array of strings. For example, you may have an issue entity property called `myConfig`
containing the following value:

    {
        "myListOfGroups": [ "developers", "support-staff" ]
    }

Then you could write the following condition to restrict the display of a module only to a user in at least one of those
groups:

    {
        "condition": "entity_property_contains_any_user_group",
        "params": {
            "entity": "issue",
            "propertyKey": "myConfig",
            "objectName": "myListOfGroups"
        }
    }

## <a name="1.2.9" href="#1.2.9">1.2.9</a>

### New Confluence context parameters

Confluence now supports the additional context parameters related to attachments:
 - `attachment.id`
 - `attachment.name`
 - `attachment.mediaType`

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.9", "1.2.9", "Connect-1.2.9", "Connect-1.2.9")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.2.8" href="#1.2.8">1.2.8</a>

### New `has_attachment` Jira Condition

The has_attachment condition can be used to check whether the currently displayed issue has any attachments. Additionally, an optional extension param may be provided to check if it has an attachment with a specified file extension (case insensitive).

For example, to check if the issue has any PDF files attached, the condition should be specified thus:

    {
      "condition": "has_attachment",
      "params": { 
        "extension": "pdf"
      }
    }

### New `entity_property_exists` Jira and Confluence Conditions

In order to solve [AC-2169](https://ecosystem.atlassian.net/browse/AC-2169) we have added a new `entity_property_exists`
condition. This condition has the following aliases:

 - `addon_property_exists`
 - `content_property_exists`
 - `space_property_exists`

This property allows you to check for the existence of an entire entity property or a single field within that entity
property. For example, if you have an issue entity property called `extraUrls` and it has the following value:

    {
        "standardUrls": [
            "http://path.to.somewhere.com",
            "http://path.to.somewhere.else.com"
        ],
        "sidebarUrls": [
            "https://www.atlassian.com",
            "https://marketplace.atlassian.com"
        ]
    }

Then you could write a condition in your descriptor that could check if the `sidebarUrls` property exists on a given
issue. That condition would look like this:

    {
        "condition": "entity_property_exists",
        "params": {
            "entity": "issue",
            "propertyKey": "extraUrls",
            "objectName": "sidebarUrls"
        }
    }

You can [use this new condition](../concepts/conditions.html#property-exists-example) to write powerful new functionality 
for your app.

### [AC-2176](https://ecosystem.atlassian.net/browse/AC-2176) User property conditions are now marked context-free

This means that user property conditions can be applied to modules that don't have a context, such as general pages.
App property conditions are also context-free, whereas all other property conditions require a context.
See [AC-2176](https://ecosystem.atlassian.net/browse/AC-2176) for more information.

### Other Changes

 - [CE-709](https://ecosystem.atlassian.net/browse/CE-709) - (Confluence) the space key is now included in the location
    context in the JavaScript API.
 - [CE-734](https://ecosystem.atlassian.net/browse/CE-734) - (Confluence) the scrollPosition API was reporting the wrong
    Y position in Firefox. This has been fixed.

### All improvements and fixes

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.7", "1.2.8", "Connect-1.2.7", "Connect-1.2.8")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.2.1" href="#1.2.1">1.2.1</a>

### New `entity_property_equal_to_context` and `entity_property_contains_context` Jira and Confluence conditions

In order to solve [AC-1608](https://ecosystem.atlassian.net/browse/AC-1608) we have added in two new conditions:

 - `entity_property_equal_to_context`
 - `entity_property_contains_context`

These conditions have the following aliases:

 - `content_property_equal_to_context` (Confluence)
 - `content_property_contains_context` (Confluence)
 - `space_property_equal_to_context` (Confluence)
 - `space_property_contains_context` (Confluence)
 - `addon_property_equal_to_context` (Jira and Confluence)
 - `addon_property_contains_context` (Jira and Confluence)

This property lets you specify a [context parameter](../concepts/context-parameters.html), from the current HTTP request,
to use for comparison against an entity property. For example, to write a condition that only evaluates to true when a
pre-defined project "Scrum Master" is the one making the HTTP request you would write something like this:

    {
        "condition": "entity_property_equal_to_context",
        "params": {
            "entity": "project",
            "propertyKey": "teamRoles",
            "objectName": "scrumMaster",
            "contextParameter": "user.key"
        }
    }

As you can see, the primary difference between the usual entity property conditions (other than the name of the condition),
is that there is no `value` field and instead that has been replaced with the `contextParameter` field. In other conditions,
the `value` field must be a JSON literal value; it will be the same value for every HTTP request across all of production
and across every tenant. Instead, for this condition, you must provide a valid Jira or Confluence
[context parameter](../concepts/context-parameters.html) inside the `contextParameter` field and that is the value that
will be used for comparison in the condition at runtime. It is important to note that the "Standard parameters" from the
context parameters cannot be used here. Only the Jira or Confluence specific parameters are allowed.

If a given `contextParameter` cannot be found in the current HTTP request context then the condition will evaluate to
false. For example, a condition that asks for the 'user.key' when there is no logged in user will result in the condition
evaluating to false.

This condition allows you to have per instance configuration that will let you dictate which modules appear to different
users based on contextual information for that request.

## <a name="1.2.0" href="#1.2.0">1.2.0</a>

### [AC-1080](https://ecosystem.atlassian.net/browse/AC-1080) Apps can act on behalf of users.

This is one of our most requested issues, and we are pleased to announce that apps may now perform server-to-server
REST calls on behalf of a specified user, using the industry standard JWT Profile for OAuth 2.0 Client Authentication and Authorization Grants.

Read more details on [developer.atlassian.com](https://developer.atlassian.com/static/connect/docs/latest/concepts/OAuth2-JWT-Bearer-Token-Authentication.html).

### All improvements and fixes

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.2.0", "Connect-1.2.0")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.119" href="#1.1.119">1.1.119</a>

### New time tracking provider and time tracking conditions

In this version we have added a new module type called `jiraTimeTrackingProviders`.
If declared in your app this module is available for selection in the time tracking admin menu
and via the time tracking [REST API](/cloud/jira/platform/rest/#api-configuration-timetracking-get):

Along with the module, we have added 2 new conditions:
- `jiraTimeTrackingProviderEnabled` which checks if the Jira provided time tracking is selected
- `addonTimeTrackingProviderEnabled` which checks if a module of type `jiraTimeTrackingProvider` with the given `addonKey` and `moduleKey` is selected

 You can use these conditions to show/hide your web fragments depending
 on the currently selected time tracking provider.

## <a name="1.1.107" href="#1.1.107">1.1.107</a>

### New `entity_property_contains_any` and `entity_property_contains_all` Jira and Confluence conditions

In order to solve [AC-1607](https://ecosystem.atlassian.net/browse/AC-1607) we have added in two new conditions:

 - `entity_property_contains_any`
 - `entity_property_contains_all`

That have the following aliases:

 - `content_property_contains_any` (Confluence)
 - `content_property_contains_all` (Confluence)
 - `space_property_contains_any` (Confluence)
 - `space_property_contains_all` (Confluence)
 - `addon_property_contains_any` (Jira and Confluence)
 - `addon_property_contains_all` (Jira and Confluence)

These conditions have the same structure as the `entity_property_equal_to` condition but the "value" field of the condition
must be a JSON Array. This is because these conditions perform a "contains" check instead of an "is equal to" check.

The `entity_property_contains_any` condition will evaluate to true if any of the values in the condition are
present in the entity property that it is being compared to. Whereas the `entity_property_contains_all` condition will
only evaluate to true if all of the values in the condition are present in the entity property.

For example, imagine that you stored the following data against a Jira project under the property key *consumingTeams*:

    {
        "teamCount": 4,
        "teams": ["Legal", "Human Resources", "Accounting", "Taxation"]
    }

You could then write a condition to only apply to projects that are for the 'Accounting' and 'Development' teams, like so:

    {
        "condition": "entity_property_contains_any",
        "params": {
            "entity": "project",
            "propertyKey": "consumingTeams",
            "objectName": "teams",
            "value": "[\"Accounting\", \"Development\"]"
        }
    }

You will note that the `value` parameter needs to be a string escaped JSON element.

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.107", "Connect-1.1.107")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.106" href="#1.1.106">1.1.106</a>

### Bug fix for Confluence macro previews

Confluence Macros previews in both macro editor and content preview no longer fail to render. This bug has been present since 1.1.101.

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.106", "Connect-1.1.106")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.105" href="#1.1.105">1.1.105</a>

### Navigator JS API functionality extended

Functionality added to the navigator method within the JavaScript API.
The `addonModule` route has been added to the navigator, which navigates to a `generalPages`
module as defined by a connect app.

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.105", "Connect-1.1.105")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.101" href="#1.1.101">1.1.101</a>

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.101", "Connect-1.1.101")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.100" href="#1.1.100">1.1.100</a>

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, CE) AND fixVersion in ("1.1.100", "Connect-1.1.100")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.99" href="#1.1.99">1.1.99</a>

### Multi-select issue field type

Connect apps can define a multi-select type of issue field.

### Executed deprecation notice of `jsonValue=true` field on App Entity Properties resource

This is now the default behaviour of the app entity property get request:

    GET /rest/atlassian-connect/1/addons/my-plugin-key/properties/my-property

    {"key":"test-property","value":{"string":"string-value","number":5},"self":"..."}

You will note that the `value` field contains a JSON object instead of a string. This deprecation was announced over
six months ago and has now been executed.

## <a name="1.1.97" href="#1.1.97">1.1.97</a>

### Bug fix for licensing endpoint

Requests to `/rest/atlassian-connect/latest/license` no longer fail.
This bug was present only in version `1.1.96`.

## <a name="1.1.96" href="#1.1.96">1.1.96</a>

### Remote conditions deprecated
Conditions which are evaluated by an app service have been deprecated. Remote conditions add a considerable performance
 penalty to rendering pages. Apps using remote conditions should transition as soon as possible to using [conditions with
 entity properties](../concepts/conditions.html#property-conditions).

For any questions or problems migrating away from remote conditions, please [drop us a line in the mailing list](../resources/getting-help.html)
and we'll make sure you're covered before remote conditions are removed.

### License (`lic`) parameter now included in web hooks
See [AC-1217](https://ecosystem.atlassian.net/browse/AC-1217) for more information.

### Number issue field type
A "number" [issue field](../modules/jira/issue-field.html) type is now supported.

### Default space logos now accessible to Connect apps

URLs to download default Confluence space logos are now whitelisted. [CE-425](https://ecosystem.atlassian.net/browse/CE-425)

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.96", "Connect-1.1.96")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.94" href="#1.1.94">1.1.94</a>

### Add support for issue fields
Connect in Jira now supports adding issue fields via the [issue fields module](../modules/jira/issue-field.html).

### Add support for blob filenames for file upload requests
This change is needed since IE and Safari seems to not have a File constructor so the polyfill is used for these browsers. [ACJS-239](https://ecosystem.atlassian.net/browse/ACJS-239)

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.94", "Connect-1.1.94", "3.1.28")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.90" href="#1.1.90">1.1.90</a>

### Shared secret length increased
New installations will now create shared secrets of at least 256 bits. [AC-1948](https://ecosystem.atlassian.net/browse/AC-1948)

### New webhooks in Confluence
Support for two new Confluence webhooks relating to attachments have been added.

* attachment_trashed: When an attachment is moved to the trash but not deleted.
* attachment_restored: When an attachment is restored from the trash.

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.90", "Connect-1.1.90")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.86" href="#1.1.86">1.1.86</a>

### Content properties for Confluence spaces and pages

Confluence apps can now store data against spaces and pages and use those properties in conditions. This can be used
to eliminate the need for remote conditions. See [conditions](../concepts/conditions.html)
for more details.

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.86", "Connect-1.1.86")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.85" href="#1.1.85">1.1.85</a>

### File upload support in JavaScript API

[`AP.request`](../javascript/module-request.html) has been updated to accept object structures, including `File` objects, when using the `multipart/form-data`
content type.

    var fileToUpload = document.getElementById("fileInput").files[0];
    AP.require('request', function(request){
      request({
        url: ...,
        type: 'POST',
        contentType: 'multipart/form-data',
        data: {file: fileToUpload}
      });
    });

This allows apps to use the attachment APIs from Jira and Confluence.

### Define dialogs in the descriptor and open them from the JavaScript API

If you have defined dialog modules in `dialogs` like so:

    "dialogs": [
      {
        "key": "fullscreen-dialog-key",
        "name": {
          "value": "Reusable fullscreen dialog"
        },
        "url": "/fullscreen-dialog-contents",
        "options": {
          "size": "fullscreen"
        }
      }
    ]

Then you can open that dialog via your apps JavaScript by referencing it by app module key directly, like so:

    AP.require('dialog', function(dialog) {
        dialog.create({
            key: 'fullscreen-dialog-key'
        });
    });

This allows you to open dialogs from anywhere in your JavaScript code; you can even open a dialog from within another dialog (nested dialogs are supported).

### New E-mail Macro render mode

A new macro render mode has been added to Confluence dynamic macros to aid e-mail notifications.

Confluence dynamic macros can be used to display interactive content and this works well for web pages. However, this does not work (and is often impossible to make work) in other contexts;
such as PDF exports and e-mail notifications. For this purpose we provide macro render modes so that when your app is asked to render a macro in the context of a PDF export or an e-mail
 notification you can display meaningful static content.

Here is an example of using the new `email` macro render mode:

    {
      "modules": {
        "dynamicContentMacros": [
          {
            "renderModes": {
              "pdf": {
                "url": "/render-map-pdf"
              },
              "email": {
                "url": "/render-map-email"
              },
              "default": {
                "url": "/render-map-static"
              }
            },
            "url": "/render-map?pageTitle={page.title}",
            "categories": [],
            "outputType": "block",
            "bodyType": "none",
            "aliases": [],
            "name": {
              "value": "Maps"
            },
            "key": "dynamic-macro-example"
          }
        ]
      }
    }

App developers can use this feature to ensure that Confluence macro's are rendered nicely in notification e-mails.

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, ACJS, CE) AND fixVersion in ("1.1.85", "Connect-1.1.85", "3.1.24")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.84" href="#1.1.84">1.1.84</a>

<div class="auto-issue-table" data-jql='(project in (AC, ACJIRA, CE) AND fixVersion in ("1.1.84", "Connect-1.1.84")) ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.81" href="#1.1.81">1.1.81</a>

### Module Descriptor for Dialogs

Dialogs can now be defined as their own modules in the app descriptor. A dialog module can be invoked from any number of Web Items,
allowing you to specify your dialog in just one place and avoid duplication. For more details, see
[the Dialog module documentation](../modules/common/dialog.html).

<div class="auto-issue-table" data-jql='project in (AC, ACJIRA, CE) AND fixVersion in ("1.1.81", "Connect-1.1.81") ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.80" href="#1.1.80">1.1.80</a>

### Apps REST resource accessible to static apps

The REST API, `/rest/atlassian-connect/1/addons/{addonKey}` can now be reached through the JavaScript API request method.

For an example, see <a href="../rest-apis/#get-addons-addonkey">apps REST APIs</a>

<div class="auto-issue-table" data-jql='project in (AC, ACJIRA, CE) AND fixVersion in ("Connect-1.1.80") ORDER BY priority DESC, component ASC, key ASC'></div>

## <a name="1.1.78" href="#1.1.78">1.1.78</a>

### New onClose trigger for AP.messages

In this release we have added a new callback to AP.messages. You can now register a function which will be invoked when the message is closed.

    AP.require("messages", function(messages){
        //create a message
        var message = messages.info('title', 'body');
        messages.onClose(message, function() {
            console.log(message, ' has been closed!');
        });
    });

For more details please see the [updated messages documentation](../javascript/module-messages.html).

We have also added the ability to hide macro parameters in the macro editor.

For more details please see the [Macro Input Parameters](../modules/fragment/macro-input-parameter.html).

<div class="auto-issue-table" data-jql="key in (ACJS-12,AC-1882,AC-1883,CE-300)"></div>

## <a name="1.1.77" href="#1.1.77">1.1.77</a>

### Overview

 * Jira and Confluence: New `objectName` property for `entity_property_equal_to` conditions [AC-1847](https://ecosystem.atlassian.net/browse/AC-1847)
 * Jira: Date picker component [ACJIRA-729](https://ecosystem.atlassian.net/browse/ACJIRA-729)
 * Jira: `has_global_permission` condition [ACJIRA-777]([https://ecosystem.atlassian.net/browse/ACJIRA-777)


### New `objectName` property for `entity_property_equal_to` conditions

In this release we have updated the `entity_property_equal_to` condition to make two things possible:

 * Using the `objectName` parameter you can now compare against a sub-field of an entity property.
 * Comparisons can be performed against non-primitive types; you can now compare two JSON objects.

For example, imagine that you create an issue entity property with the key `start-of-justice` and the value:

    {
      "isJusticeTeam": true,
      "teamMemberCount": 3,
      "teamMembers": ["Manbat", "Superdude", "Wonder person"]
    }

Then you could write a condition to ensure that you only show a web fragment when the issue is for a justice team:

    {
        "condition": "entity_property_equal_to",
        "params": {
            "entity": "issue",
            "propertyKey": "start-of-justice",
            "objectName": "isJusticeTeam",
            "value": "true"
        }
    }

Critically this means that the `teamMemberCount` and `teamMembers` JSON fields will be ignored from the perspective of
the `entity_property_equal_to` comparison.

Just as a quick reminder of the comparison rules: all comparisons must be exact, no extra elements. This means that:

 * JSON Primitive types will be compared to in the standard way
 * JSON Array types will be compared to be exactly identical in terms of size and ordering.
 * JSON Object types need to have exactly the same number of fields and those fields need to be identical. Ordering does not matter.

With this new feature you should be able to write useful and powerful conditions against entity properties.

<div class="auto-issue-table" data-jql="key in (ACJIRA-777,ACJIRA-729,AC-1847)"></div>

## <a name="1.1.76" href="#1.1.76">1.1.76</a>      

The Navigator Javascript API has been updated so that the `navigator.getLocation` callback will contain context variables relative to
the page currently open in the host application.

    AP.require('navigator', function(navigator){
      navigator.getLocation(function(context) {
        // Do something with the context object
      });
    });

For more details please see the [updated Navigator documentation](../javascript/module-Navigator.html).

Also, AP.request has been updated to include an `experimental` field that expects a boolean value. Adding the `experimental`
field to an AP.request will let you make calls successfully to experimental REST API's; this is especially useful if you wish
to use some of the latest Service Desk API's. You use the field as follows:

    AP.request({
        url: ...,
        ...
        experimental: true
    });

<div class="auto-issue-table" data-jql="key in (CE-289,CE-278,CE-220,ACJS-75)"></div>

## <a name="1.1.75" href="#1.1.75">1.1.75</a>

### New context parameters for conditions

[Inline conditions](../concepts/context-parameters.html#inline-conditions) enables the result of evaluating a web
fragment condition to be passed as a context parameter to the app in an iframe request URL, instead of simply being
used to determine whether the iframe should be loaded.

This new feature can be used to avoid making REST API requests on iframe load, or to simplify app descriptors that
currently contain duplicate modules for the positive and negative results of a web fragment condition.

### Dynamic Blueprint Variables
[Connect Blueprint Modules](../modules/confluence/blueprint.html) have been extended to allow you to define an
endpoint on your server that will provide dynamic variable substitution for blueprints as the Confluence page is created.
This feature can be used to change a blueprint depending on the user, space, etc. For more information, see
[this example](../modules/fragment/blueprint-template-context.html).

<div class="auto-issue-table" data-jql="key in (ACJIRA-778,CE-252,ACJIRA-724,AC-1825)"></div>

## <a name="1.1.71" href="#1.1.71">1.1.71</a>

### Overview

 * Confluence: Enable chromeless dialogs for custom macro editors [CE-283](https://ecosystem.atlassian.net/browse/CE-283)

<div class="auto-issue-table" data-jql="key in (CE-283)"></div>

## <a name="1.1.69" href="#1.1.69">1.1.69</a>

### Overview

 * Jira: Whitelist worklog resource (see the [Jira REST Scopes page](../scopes/jira-rest-scopes.html)) [AC-1826](https://ecosystem.atlassian.net/browse/AC-1826)

<div class="auto-issue-table" data-jql="key in (AC-1826)"></div>

## <a name="1.1.68" href="#1.1.68">1.1.68</a>

### New `addon_is_licensed` condition

In this release we have added in a new condition for Jira and Confluence. The `addon_is_licensed` condition can be placed
on any Atlassian Connect module that supports conditions. It will evaluate to `true` if and only if your app is a paid app and it
is licensed. Here is an example of the new condition in use:

    "jiraIssueTabPanels": [{
        "conditions": [
            {
                "condition": "addon_is_licensed"
            },
            {
                "condition": "user_is_logged_in"
            }
        ],
        "key": "your-module-key",
        "name": {
            "value": "Your module name"
        },
        "url": "/panel/issue?issue_id={issue.id}&issue_key={issue.key}",
        "weight": 100
    }]

In this example the Jira Issue Tab Panel will only be shown if the app is licensed and the user is logged in. There are some
caveats to the condition however:

 * If you give away your app for free then you don't need to use the `addon_is_licensed` condition. This is important because
   all free apps are considered *unlicensed* and will thus the condition will return false. Only use this condition with
   paid apps that are licensed via the Atlassian Marketplace.
 * In local development with the AMPS tools, you will likely not have a license installed that says that your app is active.
   This means that this condition will always return false. Consider only adding the condition to appropriate modules when you
   know that your app will be running on a production Atlassian Cloud instance.

### Allow multiple dialogs to be open an the same time

It is now possible open a dialog from another dialog: allowing nested dialogs in Atlassian Connect. The only caveat is that
a dialog cannot open another version of itself; this will result in an error being thrown on the client side.

### Jira Agile context parameter changes

Two new Jira Agile context parameters have been added:

 * `board.type`
 * `board.state`

Also, the `board.mode` parameter has been deprecated in favor of `board.screen`.

### Issues

<div class="auto-issue-table" data-jql="key in (AC-1641,ACJS-91,ACJIRA-593)"></div>

## [1.1.67](./1-1-0.html#1.1.67)
 * Jira: Whitelist REST APIs for Jira Service Desk [AC-1774](https://ecosystem.atlassian.net/browse/AC-1774)
 * Jira: Whitelist REST APIs for Jira Agile [ACJIRA-709](https://ecosystem.atlassian.net/browse/ACJIRA-709)
 * Allow to pass custom data to the app when opening a dialog [ACJS-10](https://ecosystem.atlassian.net/browse/ACJS-10)
 * Fullscreen Dialog with control bar at top [ACJS-83](https://ecosystem.atlassian.net/browse/ACJS-83)
 * Confluence: Add JavaScript API for browser navigation [CE-249](https://ecosystem.atlassian.net/browse/CE-249)
 * Add JavaScript API for refreshing the browser [CE-249](https://ecosystem.atlassian.net/browse/CE-249)
 * Jira: Add scope whitelisting for user properties [AC-1809](https://ecosystem.atlassian.net/browse/AC-1809)
 * Web items with JWTs fail on stale pages [ACJIRA-294](https://ecosystem.atlassian.net/browse/ACJIRA-294)

## [1.1.65](./1-1-0.html#1.1.65)
 * App Properties GET request can provide the non-escaped JSON value with a query parameter [AC-1693](https://ecosystem.atlassian.net/browse/AC-1693)
 * Jira: Added condition to allow a query for available products [ACJIRA-590](https://ecosystem.atlassian.net/browse/ACJIRA-590)

## [1.1.64](./1-1-0.html#1.1.64)
 * Jira: Two new modules for you to define global and project permissions for your Jira apps.
 * Jira Software: The 'agileBoard.id' and 'agileBoard.mode' content properties became the 'board.id' and 'board.mode' content properties respectively. And another Jira Software content property has been added 'sprint.id'.

## [1.1.60](./1-1-0.html#1.1.60)
 * Jira: Make jira.openCreateIssueDialog() available on general admin and project admin pages [AC-1652](https://ecosystem.atlassian.net/browse/AC-1652)
 * Jira: Fixed issue when installing app with ADMIN scope, affecting some versions of upstream services [AC-1771](https://ecosystem.atlassian.net/browse/AC-1771)

## [1.1.57](./1-1-0.html#1.1.57)
* Fixed web item actions being broken by certain conditions [AC-1640](https://ecosystem.atlassian.net/browse/AC-1640)
* More specific error messages shown for SSL-related installation errors [AC-1216](https://ecosystem.atlassian.net/browse/AC-1216)

## [1.1.55](./1-1-0.html#1.1.55)
* Added `post-install-page` module for apps to provide a page with information on getting started [AC-1579](https://ecosystem.atlassian.net/browse/AC-1579)
* Confluence: Added fields to the `confluenceContentProperties` module to allow apps to provide aliases for content properties [CE-155](https://ecosystem.atlassian.net/browse/CE-155)
* Confluence: Added support for content property aliases in the UI of the CQL builder [CE-156](https://ecosystem.atlassian.net/browse/CE-156)
* REST endpoint `/secure/viewavatar` accessible with READ scope [AC-1748](https://ecosystem.atlassian.net/browse/AC-1748)
* Jira: Added field to `data-options` which hides the footer on an app provided page [ACJIRA-220](https://ecosystem.atlassian.net/browse/ACJIRA-220)

## [1.1.52](./1-1-0.html#1.1.52)
* Reduce required scope for Jira project properties to WRITE [AC-1686](https://ecosystem.atlassian.net/browse/AC-1686)

## [1.1.48](./1-1-0.html#1.1.48)
* Jira: Whitelist REST API endpoint for unassigning epic from issues [ACJIRA-542](https://ecosystem.atlassian.net/browse/ACJIRA-542)

## [1.1.47](./1-1-0.html#1.1.47)
* Remove OAuth 1.0 as an authentication type in the descriptor [AC-1688](https://ecosystem.atlassian.net/browse/AC-1688)

## [1.1.42](./1-1-0.html#1.1.42)
* Jira: Whitelist REST API for Jira Agile [ACJIRA-491](https://ecosystem.atlassian.net/browse/ACJIRA-491)

## [1.1.41](./1-1-0.html#1.1.41)
* Jira: Fix issue and project permission conditions [AC-1666](https://ecosystem.atlassian.net/browse/AC-1666)

## [1.1.37](./1-1-0.html#1.1.37)
* Jira: Added ability to open Create Issue Dialog from the app [ACJIRA-113](https://ecosystem.atlassian.net/browse/ACJIRA-113)
* Jira: Added API to check if user is allowed to modify the dashboard [ACJIRA-436](https://ecosystem.atlassian.net/browse/ACJIRA-436)
* Fix non-responsive dialog chrome buttons when errors from loading content [ACJS-44](https://ecosystem.atlassian.net/browse/ACJS-44)
* Fixed javascript errors when pressing escape on a non-dialog iframe [ACJS-46](https://ecosystem.atlassian.net/browse/ACJS-46)
* Confluence: Added scope to create space with an app [CE-157](https://ecosystem.atlassian.net/browse/CE-157)
* Jira: Fixed refreshIssuePage to run on first call [AC-1599](https://ecosystem.atlassian.net/browse/AC-1599)

## [1.1.35](./1-1-0.html#1.1.35)
* Fixed broken dialog button callbacks [ACJS-41](https://ecosystem.atlassian.net/browse/ACJS-41)

## [1.1.33](./1-1-0.html#1.1.33)
* Confluence: Fixed broken generation of macroHash [CE-163](https://ecosystem.atlassian.net/browse/CE-163)

## [1.1.32](./1-1-0.html#1.1.32)
* Add `entity_property_equal_to` condition for app properties [ACJIRA-362](https://ecosystem.atlassian.net/browse/ACJIRA-362)
* Jira: Add `entity_property_equal_to` condition for project, issue, issue type and comment properties [ACJIRA-362](https://ecosystem.atlassian.net/browse/ACJira-362)
* Jira: Provide dashboard items [ACJIRA-248](https://ecosystem.atlassian.net/browse/ACJIRA-248)
* Confluence: Add view mode parameter for blueprints [CE-110](https://ecosystem.atlassian.net/browse/CE-110)
* Jira: Add issue type ID context parameter for issue tab panels [ACJIRA-417](https://ecosystem.atlassian.net/browse/ACJIRA-417)
* Confluence: Add macro.id context parameter for macros [CE-79](https://ecosystem.atlassian.net/browse/CE-79)
* Jira: Whitelist REST API endpoints for dashboard items [ACJIRA-247](https://ecosystem.atlassian.net/browse/ACJIRA-247)
* Jira: Whitelist REST API endpoints for project roles [ACJIRA-394](https://ecosystem.atlassian.net/browse/ACJIRA-394)
* Client-side JWT refresh fails for users with UTF-8 characters in the display name [ACJS-36](https://ecosystem.atlassian.net/browse/ACJS-36)
* Dialog close button doesn't work if there is an error loading the contents [ACJS-6](https://ecosystem.atlassian.net/browse/ACJS-6)
* Hotkeys cannot be used in dialogs until focused by the user [ACJS-22](https://ecosystem.atlassian.net/browse/ACJS-22)
* Confluence: : = | RAW | = : parameter causes errors in macros [AC-1573](https://ecosystem.atlassian.net/browse/AC-1573)
* Jira: Admin pages lose navigation context [ACJIRA-114](https://ecosystem.atlassian.net/browse/ACJIRA-114)
* Errors in the JavaScript API are swallowed [ACJS-2](https://ecosystem.atlassian.net/browse/ACJS-2)

## [1.1.29](./1-1-0.html#1.1.29)
* Username and display name available on the JWT token [AC-1558](https://ecosystem.atlassian.net/browse/AC-1558)
* Autoconvert: Limiter on number of possible patterns for a single macro

## [1.1.27](./1-1-0.html#1.1.27)
* Confluence: Allow index schema configuration for content properties [CE-77](https://ecosystem.atlassian.net/browse/CE-77)
* Confluence: Support for extending autoconvert for macros [CE-33](https://ecosystem.atlassian.net/browse/CE-33)
* Jira: Whitelist REST API methods for JQL auto-complete suggestions [ACJIRA-367](https://ecosystem.atlassian.net/browse/ACJIRA-367)
* Improve feedback for failed app installations through UPM [AC-1547](https://ecosystem.atlassian.net/browse/AC-1547)

## [1.1.25](./1-1-0.html#1.1.25)
* Add REST API for storing and accessing app properties [ACJIRA-28](https://ecosystem.atlassian.net/browse/ACJIRA-28)
* Jira: Whitelist REST API methods for comment properties [ACJIRA-306](https://ecosystem.atlassian.net/browse/ACJIRA-306)
* Confluence: confluence.closeMacroEditor() stopped working [AC-1525](https://ecosystem.atlassian.net/browse/AC-1525)

## [1.1.23](./1-1-0.html#1.1.23)
 * Jira: Fixed broken JavaScript API method - jira.refreshIssuePage() [ACDEV-1508](https://ecosystem.atlassian.net/browse/AC-1508)

## [1.1.22](./1-1-0.html#1.1.22)
* Fixed error which prevented web-items with remote conditions from loading [AC-1503](https://ecosystem.atlassian.net/browse/AC-1503)
* Confluence: Fixed error which prevented retrieving a macro body by its hash [AC-1505](https://ecosystem.atlassian.net/browse/AC-1505)

## [1.1.21](./1-1-0.html#1.1.21)
* Confluence: Added support for render modes for Dynamic Content Macros [CE-66](https://ecosystem.atlassian.net/browse/CE-66)
* Jira: Added support for aliases in Entity Properties [ACJIRA-250](https://ecosystem.atlassian.net/browse/ACJIRA-250)
* Jira: Added context parameters for Agile Boards [ACJIRA-272](https://ecosystem.atlassian.net/browse/ACJIRA-272)
* Improved REST API for license information [AC-1370](https://ecosystem.atlassian.net/browse/AC-1370)
* Confluence: Fixed error when inserting Connect macros in Internet Explorer [CE-74](https://ecosystem.atlassian.net/browse/CE-74)
* Jira: Whitelisted endpoints for thumbnails and user avatars [AC-1472](https://ecosystem.atlassian.net/browse/AC-1472)
* Jira: Fixed `has_issue_permission` or `has_project_permission` for web items [ACJIRA-263](https://ecosystem.atlassian.net/browse/ACJIRA-263)
* Jira: Fixed rendering strategy for dialogs when JWT token expired [ACJIRA-275](https://ecosystem.atlassian.net/browse/ACJIRA-275)
* Enabled installation of apps without modules [AC-1439](https://ecosystem.atlassian.net/browse/AC-1439)

## [1.1.18](./1-1-0.html#1.1.18)
* Fixed: Confluence macro editor replaces page content after it gets closed in Internet Explorer

## [1.1.17](./1-1-0.html#1.1.17)
* Confluence: fixed a bug that made chromeless dialogs fail to display [AC-1449](https://ecosystem.atlassian.net/browse/AC-1449)
* Jira: Expose system properties with REST API: [ACJIRA-123](https://ecosystem.atlassian.net/browse/ACJIRA-123)
* Jira: Whitelist Jira Agile REST calls for getting epics and adding issues to epics: [ACJIRA-219](https://ecosystem.atlassian.net/browse/ACJIRA-219)

## [1.1.15](./1-1-0.html#1.1.15)
* Improvements in lifecycle webhook signatures.
* Fixed: SSL issues for apps hosted on OpenShift.

## [1.1.10](./1-1-0.html#1.1.10)
* Fixed: app loaded twice in the Issue Navigator
* Fixed: adminpage iframe has padding / margin
* Send Connect version information to the app
* Scopes white-list Jira user picker API

## [1.1.9](./1-1-0.html#1.1.9)
* First phase for [Blueprints](../modules/confluence/blueprint.html)

## [1.1.8](./1-1-0.html#1.1.8)
* Fixed: duplicate web panels in Jira search-for-issues results
* Fixed: iFrame resizing in Chrome
* Better error message when group permissions prevent app installation

## [1.1.7](./1-1-0.html#1.1.7)
* Bug fixes and minor improvements

## [1.1.6](./1-1-0.html#1.1.6)
* More [Jira reports](../modules/jira/report.html) features.
* Apps are no longer automatically uninstalled when the installation lifecycle hook returns an error response.

## [1.1.4](./1-1-0.html#1.1.4)
* Confluence now supports `content.*` variables everywhere that `page.*` variables were supported.
* Support for [Jira reports](../modules/jira/report.html)
* Dialogs and inline dialogs will no longer suffer from expired JWTs.
* Apps will no longer change from disabled to enabled as a result of automatic updates.

## [1.1.0-rc.4](./1-1-0.html#rc4)
* Bug fixes and stability improvements

## [1.1.0-rc.3](./1-1-0.html#rc3)
* __Read about [breaking changes](./1-1-0.html#breaking-changes)__
* Fixed OAuth authenticated requests
* Temporarily removed support for Confluence Mobile

## [1.1.0-rc.2](./1-1-0.html#rc2)
* __Read about [breaking changes](./1-1-0.html#breaking-changes)__
* Whitelist Confluence Questions context params for webitems and webpanels

## [1.1.0-rc.1](./1-1-0.html#rc1)
* __Read about [breaking changes](./1-1-0.html#breaking-changes)__
* JavaScript API: removed deprecated function `AP.fireEvent()`
* New feature: condition for checking dark features
* Fixed: web-sections which rely on app-provided web-items fail to register
* Valid module keys are no longer modified
* Introduced Confluence Questions `content.id` / `content.version` / `content.type` / `content.plugin` [context parameters](../concepts/context-parameters.html) for webitems and webpanels
* Docs: Fix broken links to webhooks module page
* Docs: web panels are not to be used for dialog content

## [1.1.0-beta.5](./1-1-0.html#beta-5)
* __Read about [breaking changes](./1-1-0.html#breaking-changes)__
* XML descriptor servlet paths redirect to new JSON descriptor paths
* Manage cookies through the [javascript cookie api](../javascript/module-cookie.html)
* Manage browser history through the [javascript history api](../javascript/module-history.html)
* Improve modal dialog, to introduce the chrome flag
* Dialog height is now always the height of the iframe
* Refresh a Jira issue without reloading the page
* Fixed bug that allowed content to appear in wrong iframe
* Big speed improvements on app install, enable, disable, uninstall etc.
* Additional REST paths added to scopes and whitelists
* App keys and app module keys have stricter restrictions
* Fixed numerous workflow post function bugs
* Improved documentation for workflow post functions
* Remote web panels fixed in Jira Agile

## [1.0.2](./1-0-2.html)
* Fixed workflow post functions
* Support for hidden macros in the macro browser
* Removing content from Web-panel no longer leaves grey bar in place

## [1.0.1](./1-0-1.html)
* Allow POST method for "screens/addToDefault/{fieldId}"
* UPM auto-update fails to upgrade app

## [1.0.0](./1-0.html)
* Installing an app into OnDemand will not work unless the base url starts with https
* Support for context parameters on remote conditions
* The app key must now be less than or equal to 80 characters. Any apps with larger keys will need to be shortened
* Module `key` attributes are now required
* WebPanel url and location fields are now required
* Only apps with a [`baseUrl`](../modules#baseUrl) starting with ``https://`` can be installed in OnDemand servers. ``http://`` may still be used for testing locally.
* Increased [security](../concepts/security.html): apps are assigned a user in each product instance in which they are installed and server-to-server requests go through authorisation checks as this user.
* Fixes issue where `user_is_logged_in` condition caused general pages to not be viewable
* Fixes numerous issues with context parameters not being sent through to conditions and pages
* Removes page header from Confluence general pages

## [1.0-m31](./1-0-m31.html)
* Support for Inline Dialogs
* The [`authentication`](../modules/authentication.html) module is now required
* Apps that request JWT authentication will now fail to install if they do not specify an ``"installed"``
[lifecycle callback](../modules/lifecycle.html). To opt out of JWT authentication, you may specify an authentication
type of ``"none"``.

## [1.0-m30](./1-0-m30.html)
* Removal of email sending resource
* Support for [Jira issue properties](../modules/jira/entity-property.html)
* Make [AP.messages](../javascript/module-messages.html) API stable
* Whitelisted remote endpoints are [listed in the documentation](../scopes/scopes.html)
* Fix bug with OAuth and JSON descriptor

Read the [1.0-m30 release notes](./1-0-m30.html).

## [1.0-m29](./1-0-m29.html)
* Tabs can now be added to Confluence Space Tools section. Check out [Space Tools Tab](../modules/confluence/space-tools-tab.html)
documentation for more information
* Support for [web sections](../modules/common/web-section.html)
* Support for full screen dialogs
* AC Play support for JSON descriptor and JWT. Read the [upgrade guide](../guides/upgrade-play.html)

Read the [1.0-m29 release notes](./1-0-m29.html).

## [1.0-m28](./1-0-m28.html)
* New documentation for the Atlassian Connect Javascript API
* Java 7 is no longer required at runtime (change in atlassian-jwt 1.0-m8)
* JSON descriptors that request web-hooks must now also request the corresponding scopes required to receive these web-hooks
    * Without the correct scope you will see an error in the host product's log during installation that tells you which scope to add
* Jira REST API endpoints are in the JSON descriptor scopes white-list
    * If your app uses these endpoints then you can now specify scopes in your descriptor and they will be respected in authorisation checks on requests to Jira
    * E.g. add ```"scopes": ["READ", "WRITE"]``` to your JSON descriptor if your app performs read-only actions and mutating actions
    * Scopes white-list documentation coming soon so that you will be able to figure out which scope is required for each endpoint that you access

Read the [1.0-m28 release notes](./1-0-m28.html).

## [1.0-m27](./1-0-m27.html)

* Support for Macro image placeholder values
* Support for long query parameters for remote macros
* `web-item` module `link` attribute renamed to `url`
* Fixed bug which prevented incoming JWT requests from being accepted
* Fixed the configure page url with JSON descriptor
* Better error reporting and bug fixes for JSON descriptor
* Docs are now available _in product_. Just visit `https://HOSTNAME:PORT/CONTEXT_PATH/atlassian-connect/docs/`

Additionally, we have relaxed the deprecation period for the XML descriptor until __28th February, 2014__.

Read the [1.0-m27 release notes](./1-0-m27.html).

## [1.0-m25](./1-0-m25.html)
Atlassian Connect `1.0-m25` introduces a number of changes to how you will build apps for
Atlassian OnDemand. There are two important changes in this release: a new format for your app
descriptor and a new authentication method. Both of these changes are designed to help developers
build apps more quickly and easily.

* JSON App Descriptor
* JSON Web Token (JWT) Authentication
* atlassian-connect-express `v0.9.0`

These new features replace the XML descriptor and OAuth, which are now deprecated. Please read the
[deprecation notices](../resources/deprecations.html).

Read the [1.0-m25 release notes](./1-0-m25.html).

## Earlier releases
For earlier release notes, please see the [Atlassian Connect Blog](https://developer.atlassian.com/pages/viewrecentblogposts.action?key=AC).